<?php
 
class DB {

private $dbHost;
private $dbPort;
private $dbName;
private $dbUser;
private $dbPass;
private $dbConn;

public function __construct(){
    $this->dbHost = 'localhost';
    $this->dbPort = '3306';
    $this->dbName = 'web2a';
    $this->dbUser = 'jesurythomas';
    $this->dbPass = 'usjr123';
}

public function connect(){
    $this->dbConn = new PDO("mysql:host=".$this->dbHost.";dbname=".$this->dbName.";port=".$this->dbPort."};",$this->dbUser,$this->dbPass);
    $this->dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
 
public function getQuery($sql){
    $query=$this->dbConn->prepare($sql);
    $query->execute();
    $row = $query ->fetchall();
    return $row;
}
 
public function setQuery($sql){
    $query=$this->dbConn->prepare($sql);
    $query->execute();
}

}
?>
