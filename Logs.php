<?php
require_once("script_driver_DB.php");
class Logs
{

    public function __construct()
    {
    }

    public function get_logs()
    {
        $log_query = "SELECT * from logs order by _id desc;";
        $DBL = new DB();
        $DBL->connect();
        $results = $DBL->getQuery($log_query);
        $html = '<table>';
        foreach ($results as $logData) {
            $html .= '<tr><td>' . $logData['_id'] . '</td>' . '<td>' . $logData['log'] . '</td>' . '<td>' . $logData['session_id'] . '</td></tr>';
        }
        $html .= '</table>';
        echo $html;
    }

    public function send_log($logMessage, $actionType, $actor)
    {
        $log_query = "INSERT INTO logs (log,action_type,actor_id) VALUES ('" . $logMessage . "','" . $actionType . "','" . $actor . "')";
        $DBL = new DB();
        $DBL->connect();
        $results = $DBL->setQuery($log_query);
    }
}


class LogsCombat
{

    public function __construct()
    {
    }

    public function get_logs($session_id)
    {
        $log_query = "SELECT * from combat_logs where session_id = " . $session_id . " order by _id desc;";
        $DBL = new DB();
        $DBL->connect();
        $results = $DBL->getQuery($log_query);
        $html = '<table>';
        foreach ($results as $logData) {
            $html .= '<tr><td>' . $logData['_id'] . '</td>' . '<td>' . $logData['log'] . '</td></tr>';
        }
        $html .= '</table>';
        echo $html;
    }

    public function send_log($logMessage, $actionType, $actor, $session_id=1)
    {
        $log_query = "INSERT INTO combat_logs (log,action_type,actor_id,session_id) VALUES ('" . $logMessage . "','" . $actionType . "','" . $actor . "','" . $session_id . "')";
        $DBL = new DB();
        $DBL->connect();
        $results = $DBL->setQuery($log_query);
    }
}
