<?php

class DBLibrary{

    private $db;
    private $sql;

    private $whereInstanceCounter = 0;
    private $valueBag = [];
    private $table;
    private $insertValues;

    public function __construct($driver, $user, $password, $options=null){
        try{
            $this->db = new PDO($driver,$user,$password);
        } catch(PDOException $error){
            echo $error->getMessage();
        }
    }

    public function select(Array $fieldList=null){
        $this->sql .= 'SELECT ';
        
        if($fieldList === null){
             $fieldList = '*';
             $this->sql .= $fieldList;
        } else {
             $contents = count($fieldList)-1;
             $count = 0; 
             foreach($fieldList as $field){
                $this->sql .= $field;  
                if($count < $contents)
                   $this->sql .= ', ';
                   
                $count++;   
             }  
        }

        $this->sql .= ' ';

        return $this;
    }

    public function table($table){
        $this->table = $table;
        return $this;   
    }


    public function from($table){
        $this->sql .= 'from '.$table;
        return $this;
    }

    public function get(){
        $recordset = $this->_runGetQuery(__METHOD__); 
        return $recordset;
    }

    public function getAll(){    
        $recordset = $this->_runGetQuery(__METHOD__);  
        return $recordset;
    }

    private function _runGetQuery($getMethod){
        $this->sql .= ';';
        $dbStatement = $this->db->prepare($this->sql);
        
        if($this->valueBag){
           $dbStatement->execute($this->valueBag);
           $this->valueBag = [];
        }
        else
           $dbStatement->execute();   
        
        if($getMethod === 'DBLibrary::get')
           $recordset = $dbStatement->fetch(PDO::FETCH_BOTH); 
        elseif($getMethod === 'DBLibrary::getAll')   
           $recordset = $dbStatement->fetchAll(PDO::FETCH_BOTH);

        $whereInstanceCounter = 0;

        return $recordset;
    }

    public function where(){
        if(func_num_args() <= 1){
            throw new Exception('Expecting 2 to 3 parameters. Less than 2 parameters encountered.');
        }

        if(func_num_args() == 2){
            $field = func_get_arg(0);
            $operator = '=';
            $value = func_get_arg(1);
        } elseif(func_num_args() == 3) {
            $field = func_get_arg(0);
            $operator = func_get_arg(1);
            $value = func_get_arg(2);            
        } 
            
        $this->_runWhere($field, $operator, $value, __METHOD__);

        return $this;
    }

    public function whereOr(){
        if(func_num_args() <= 1){
            throw new Exception('Expecting 2 to 3 parameters. Less than 2 parameters encountered.');
        }

        if(func_num_args() == 2){
            $field = func_get_arg(0);
            $operator = '=';
            $value = func_get_arg(1);
        } elseif(func_num_args() == 3) {
            $field = func_get_arg(0);
            $operator = func_get_arg(1);
            $value = func_get_arg(2);            
        }

        $this->_runWhere($field, $operator, $value, __METHOD__);

        return $this; 
    }

    private function _runWhere($field, $operator, $value, $whereMethod){

        if($this->whereInstanceCounter > 0){
            if($whereMethod === 'DBLibrary::where')
                $this->sql .= ' and ';
            elseif($whereMethod === 'DBLibrary::whereOr')
                $this->sql .= ' or ';

            $this->sql .= $field.' '.$operator.' ?';
        } else {
            $this->sql .= ' where '.$field.' '.$operator.' ?';
        }
        
        $this->valueBag[] = $value;
        $this->whereInstanceCounter++;
    }

    public function showQuery(){
        return $this->sql;
    }

    public function showValueBag(){
        return $this->valueBag;
    }



}