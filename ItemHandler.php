<?php
if (!isset($_SESSION))
    session_start();
class ItemHandler
{
    private $potions;
    private $bombs;

    public function __construct($potions, $bombs)
    {
        $this->potions = $potions;
        $this->bombs = $bombs;
    }

    public function addBomb()
    {
        $this->bombs += 1;
    }

    public function addPotion()
    {
        $this->potions += 1;
    }

    public function useBomb()
    {
        $this->bombs -= 1;
    }
    public function usePotion()
    {
        $this->potions -= 1;
    }

    public function getItems()
    {
        return ["potions" => $this->potions, "bombs" => $this->bombs];
    }
}
