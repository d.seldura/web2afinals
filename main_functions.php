<?php

function static_sprite($tileNumber, $image, $name)
{
    if ($tileNumber % 8) {
        $x = $tileNumber % 8 - 1;
        $y = 7 - intval($tileNumber / 8);
    } else {
        $x = 7;
        $y = 8 - intval($tileNumber / 8);
    }
    sprite($x, $y, $image, $name, 2);
}


function collissionDetectionPrinting()
{
    print_r("<br>");
    print_r(" UP IS ");
    print_r(var_dump($_SESSION['up']));
    print_r("<br>");
    print_r(" DOWN IS ");
    print_r(var_dump($_SESSION['down']));
    print_r("<br>");
    print_r(" RIGHT IS ");
    print_r(var_dump($_SESSION['right']));
    print_r("<br>");
    print_r(" LEFT IS ");
    print_r(var_dump($_SESSION['left']));
    print_r("<br>");
}

function enterNextMap($tileNumber)
{
    $mapData = getMapTileData($tileNumber);
    $portal = 0;
    $proceed = '';
    if ($mapData['metadata'] == 'encounter') {
        if ($mapData['encounter_type'] == '1')
            $portal = 1;
        else {
            $portal = 0;
            $proceed = '';
        }

        if ($tileNumber > 1 && $portal == 1) {
            if ($_SESSION['map'] == 3) {
                $_SESSION['map'] = $_SESSION['map'] + 1;
                echo '<script>
            alert("Congratulations! You have obtained the sands of time. They send you back to the time prior to meeting the Prince of Persia AYAYAYAYAYAYYAA Pasar na mi sir?");
            window.location.assign("index.php");</script>';
            }
            if ($_SESSION['hasKey'] == 1) {
                $proceed = '';
                $_SESSION['marioPosition'] = 1;
                $_SESSION['marioSpriteX'] = 0;
                $_SESSION['marioSpriteY'] = 7;
                $_SESSION['hasKey'] = 0;

                if ($_SESSION['map'] < 3) {
                    $_SESSION['map'] = $_SESSION['map'] + 1;
                    echo '<script>window.location.reload()</script>';
                }
            } else
                $proceed = 'Cannot proceed -- no key';
                echo '<script>alert("A force field is in your way. You probably need a key.");</script>';
        }


        $_SESSION['proceed'] = $proceed;
    }
}
function encounterDetection($tileNumber)
{
    $mapData = getMapTileData($tileNumber);
    if ($mapData['encounter_type'] == '5') {
        $_SESSION['hasKey'] = 1;
    }

    $_SESSION['encounter'] = $mapData['encounter_type'];
}

function collisionDetection($tileNumber)
{
    $up = $tileNumber - 8;
    $down = $tileNumber + 8;
    $left = $tileNumber - 1;
    $right = $tileNumber + 1;
    $mapData = '';

    if ($up < 0) {
        $up = false;
    } else {
        $mapData = getMapTileData($up);
        if ($mapData['metadata'] == 'wall') $up = false;
        else $up = true;
    }

    if ($down > 64) {
        $down = false;
    } else {
        $mapData = getMapTileData($down);
        if ($mapData['metadata'] == 'wall') $down = false;
        else $down = true;
    }

    if ($right % 8 == 1) {
        $right = false;
    } else {
        $mapData = getMapTileData($right);
        if ($mapData['metadata'] == 'wall') $right = false;
        else $right = true;
    }

    if ($left % 8 == 0) {
        $left = false;
    } else {
        $mapData = getMapTileData($left);
        if ($mapData['metadata'] == 'wall') $left = false;
        else $left = true;
    }

    $_SESSION['up'] = $up;
    $_SESSION['down'] = $down;
    $_SESSION['left'] = $left;
    $_SESSION['right'] = $right;
} //end



function getMapTileData($tileNumber)
{
    $i = 1;
    foreach ($_SESSION['map_data'] as $rows) {
        if ($i == $tileNumber) return $rows;
        $i++;
    }
}

function sprite($posX, $posY, $image, $name, $index)
{
    $left = ($posX * 80);
    $top = (80 * 7) - ($posY * 80);
    $css = "<style> #" . $name . "{
        margin: 0;
        padding: 0;
        position: absolute;
        width: 80px;
        height: 80px;
        left:" . $left . "px;
        top: " . $top . "px;
        z-index:" . $index . ";
    }</style>";
    $sprite = '<div id="' . $name . '"><img src="' . $image . '"></div>';
    echo $css . $sprite;
}

function logs()
{
    $log_query = "SELECT * from logs order by _id desc;";
    $DBL = new DB();
    $DBL->connect();
    $results = $DBL->getQuery($log_query);
    $html = '<table>';
    foreach ($results as $logData) {
        $html .= '<tr><td>' . $logData['_id'] . '</td>' . '<td>' . $logData['log'] . '</td>' . '<td>' . $logData['actor_id'] . '</td></tr>';
    }
    $html .= '</table>';
    echo $html;
}

function generate_map()
{

    $nonPassableTile = '<img src="tile_sandstone_brick.png" height="80" width="80">';
    $passableTile = '<img src="tile_stone_block.png" height="80" width="80">';
    $encounterTile = '<img src="tile_stone_block_encounter.png" height="80" width="80">';
    $map = '<table><tr>';
    $i = 1;

    foreach ($_SESSION['map_data'] as $rows) {
        $path = $rows["metadata"];
        if ($path == "wall")
            $image = $nonPassableTile;
        else if ($path == 'encounter') {
            if ($rows['encounter_type'] == 1) {
                $image = $passableTile;
                static_sprite($i, "map_sprite_portal.png", "portal" . $i);
            } else $image = $encounterTile;
        } else $image = $passableTile;
        if ($i % 8 == 0) $map .= '<td><div class="mapTile">' . $image . '</div></td></tr>';
        else  $map .= '<td><div class="mapTile">' . $image . '</div></td>';
        $i++;
    }
    $map .= '</tr></table>';
    echo $map;
}

class Image
{
    private $image;
    private $id;
    private $x;
    private $y;
    private $html;

    function __construct($image, $id, $x, $y)
    {
        $this->image = $image;
        $this->id = $id;
        $this->x = $x;
        $this->y = $y;
    }

    function render()
    {

        $css = "<style> #" . $this->id . "{
        margin: 0;
        padding: 0;
        position: absolute;
        width: 80px;
        height: 80px;
        left:" . $this->x . "px;
        top: " . $this->y . "px;
    }</style>";
        $this->html = '<div id="' . $this->id . '"><img src="' . $this->image . '"></div>';
        echo $css . $this->html;
    }
}
