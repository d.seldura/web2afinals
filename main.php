<?php
session_start();
require_once("Combatant.php");
require_once("ItemHandler.php");
require_once("Encounter.php");
require_once("script_driver_DB.php");
require_once("main_functions.php");


if (!isset($_SESSION["gameStart"])) {
  $_SESSION['items'] = serialize(new ItemHandler(1, 1));
  $_SESSION['protagonist'] = serialize(new Combatant(50, 50, 15, 8, "Mario", "combat_sprite_ready.png"));
  $_SESSION["gameStart"] = 1;
  $_SESSION['marioPosition'] = 1;
  $_SESSION['marioSpriteX'] = 0;
  $_SESSION['marioSpriteY'] = 7;
  $_SESSION['map'] = 1;
  $_SESSION['current_map'] = 1;
  $_SESSION['hasKey'] = 0;
  $_SESSION['session_id'] = 1;
}

if (
  !isset($_SESSION['map_data']) ||
  $_SESSION['map'] != $_SESSION['current_map']
) {
  $DB = new DB();
  $DB->connect();
  $sql = "SELECT * FROM map_data WHERE map =" . $_SESSION['map'] . " ORDER BY map_identifier ASC";
  $_SESSION['map_data'] = $DB->getQuery($sql);
  $_SESSION['current_map'] = $_SESSION['map'];
}


enterNextMap($_SESSION['marioPosition']);
collisionDetection($_SESSION['marioPosition']);
encounterDetection($_SESSION['marioPosition']);
$Enc = new Encounter(unserialize($_SESSION['protagonist']), unserialize($_SESSION['items']));
$Enc->evaulateEncounter($_SESSION['encounter']);
$_SESSION['protagonist'] = serialize($Enc->getObjects()['Mario']);
$_SESSION['items'] = serialize($Enc->getObjects()['CurrentItems']);

?>
<style>
  #logs {
    margin: 0;
    padding: 10;
    position: absolute;
    width: 505px;
    height: 354px;
    left: 780px;
    top: 48px;
    overflow: auto;
    scrollbar-color: orange lightyellow;
  }

  #log-item {

    font-family: Arial, Helvetica, sans-serif;
  }

  .mapTile {
    margin: 0;
  }
</style>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Super Mario and The Sand That Got Away</title>
  <link rel="stylesheet" href="main_css.css">
</head>

<body>
  <div id="map">
    <?php
    sprite($_SESSION['marioSpriteX'], $_SESSION['marioSpriteY'], "sprite_mario.png", "mario", 3);
    generate_map();
    ?>
  </div>
  <div id="status-hp"><?php echo unserialize($_SESSION['protagonist'])->getstats()['hp'] ?></div>
  <div id="status-potion"><?php echo unserialize($_SESSION['items'])->getItems()['potions'] ?>/3</div>
  <div id="status-bomb"><?php echo unserialize($_SESSION['items'])->getItems()['bombs'] ?>/3</div>
  <div id="controls">



    <?php
    if ($_SESSION['left'] == false) {
      echo '<a href="#"><img src="controls_left.png" opacity:0.5></a>';
    } else
      echo '<a href="script_action_moveLeft.php" onClick= moveLeft()><img src="controls_left.png"></a>';

    if ($_SESSION['up'] == false) {
      echo '<a href="#"><img src="controls_up.png" opacity:0.5></a>';
    } else
      echo '<a href="script_action_moveUp.php" onClick= moveUp()><img src="controls_up.png"></a>';


    if ($_SESSION['hasKey'])
      echo '<a href="http://"><img src="controls_key.png"></a>';
    else echo '<a href="http://"><img src="controls_key_disabled.png"></a>';

    if ($_SESSION['down'] == false) {
      echo '<a href="#"><img src="controls_down.png" opacity:0.5></a>';
    } else
      echo '<a href="script_action_moveDown.php  " onClick= moveDown()><img src="controls_down.png"></a>';


    if ($_SESSION['right'] == false) {
      echo '<a href="#"><img src="controls_right.png" opacity:0.5></a>';
    } else
      echo '<a href="script_action_moveRight.php  " onClick= moveRight()><img src="controls_right.png"></a>';

    ?>

    <script>
      window.onkeydown = function(event) {
        // up 38
        // left 37
        // down 40
        // right 39
        if (event.keyCode == 37) {
          window.location.href = "script_action_moveLeft.php";
        }
        if (event.keyCode == 38) {
          window.location.href = "script_action_moveUp.php";
        }
        if (event.keyCode == 39) {
          window.location.href = "script_action_moveRight.php";
        }
        if (event.keyCode == 40) {
          window.location.href = "script_action_moveDown.php";
        }
      }
    </script>

  </div>
  <div id="logs">
    <div id="log-item">
      <?php
      // echo ($_SESSION['proceed']);
      // echo '</br>';
      // echo ($_SESSION['encounter']);
      // echo '</br>';
      // echo ($_SESSION['marioPosition']);
      // collissionDetectionPrinting();
      logs();
      ?>
    </div>
  </div>
</body>

</html>