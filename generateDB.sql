create database web2a;
use web2a;
create table mobs (
id int(1) auto_increment not null primary key,
isBoss int(1) default 0,
mobName varchar(32),
hp int(2),
attack int(2),
defense int(2),
image varchar(255)
);
insert into mobs (isBoss, mobName, hp, attack, defense, image) values
(0, 'GOOMBA', 35, 8,5, 'enemy_sprite_1.png'),
(0, 'Magikoompa', 25, 10,3, 'enemy_sprite_2.png'),
(0, 'Koompaling', 20, 15,9, 'enemy_sprite_3.png'),
(1, '[BOSS] BOWSER', 120, 25,12, 'boss_sprite_bowser.png'),
(1, '[BOSS] BOWSER Jr.', 100, 18,9, 'boss_sprite_bowserjr.png'),
(1, '[BOSS] Kremling King', 100, 20,9, 'boss_sprite_kremling.png');
select * from mobs;

create table map_data (
_id int(2) not null auto_increment primary key,
encounter_type int(1) not null default 0,
map int(2) not null,
metadata varchar(16) default 'wall',
map_identifier int(1) );

INSERT INTO map_data (map, metadata, map_identifier, encounter_type) VALUES (1,'encounter',1,1), (1,'path',2,0), (1,'path',3,0), (1,'encounter',4,2), (1,'wall',5,0), (1,'wall',6,0), (1,'encounter',7,4), (1,'wall',8,0),
(1,'wall',9,0), (1,'wall',10,0), (1,'wall',11,0), (1,'path',12,0), (1,'wall',13,0), (1,'wall',14,0), (1,'path',15,0), (1,'wall',16,0),
(1,'wall',17,0), (1,'encounter',18,2), (1,'path',19,0), (1,'path',20,0), (1,'encounter',21,2), (1,'path',22,0), (1,'path',23,0), (1,'wall',24,0),
(1,'wall',25,0), (1,'path',26,0), (1,'encounter',27,2), (1,'wall',28,0), (1,'wall',29,0), (1,'wall',30,0), (1,'wall',31,0), (1,'wall',32,0),
(1,'wall',33,0), (1,'path',34,0), (1,'wall',35,0), (1,'path',36,0), (1,'path',37,0), (1,'encounter',38,2), (1,'path',39,0), (1,'encounter',40,5),
(1,'wall',41,0), (1,'encounter',42,4), (1,'encounter',43,2), (1,'path',44,0), (1,'wall',45,0), (1,'wall',46,0), (1,'wall',47,0), (1,'wall',48,0),
(1,'wall',49,0), (1,'wall',50,0), (1,'wall',51,0), (1,'path',52,0), (1,'wall',53,0), (1,'wall',54,0), (1,'wall',55,0), (1,'wall',56,0),
(1,'wall',57,0), (1,'wall',58,0), (1,'wall',59,0), (1,'path',60,0), (1,'path',61,0), (1,'encounter',62,3), (1,'path',63,0), (1,'encounter',64,1);


INSERT INTO map_data (map, metadata, map_identifier, encounter_type) VALUES
(2,'encounter',1,1),(2,'path',2,0),(2,'encounter',3,2),(2,'path',4,0),(2,'wall',5,0),(2,'encounter',6,4),(2,'wall',7,0),(2,'wall',8,0),
(2,'wall',9,0),(2,'wall',10,0),(2,'wall',11,0),(2,'path',12,0),(2,'wall',13,0),(2,'path',14,0),(2,'wall',15,0),(2,'wall',16,0),
(2,'wall',17,0),(2,'encounter',18,4),(2,'encounter',19,2),(2,'path',20,0),(2,'path',21,0),(2,'path',22,0),(2,'wall',23,0),(2,'encounter',24,4),
(2,'encounter',25,5),(2,'wall',26,0),(2,'wall',27,0),(2,'wall',28,0),(2,'wall',29,0),(2,'encounter',30,2),(2,'wall',31,0),(2,'path',32,0),
(2,'encounter',33,3),(2,'wall',34,0),(2,'wall',35,0),(2,'wall',36,0),(2,'wall',37,0),(2,'path',38,0),(2,'encounter',39,2),(2,'path',40,0),
(2,'path',41,0),(2,'path',42,0),(2,'path',43,0),(2,'path',44,0),(2,'path',45,0),(2,'path',46,0),(2,'wall',47,0),(2,'wall',48,0),
(2,'encounter',49,4),(2,'wall',50,0),(2,'encounter',51,2),(2,'wall',52,0),(2,'wall',53,0),(2,'wall',54,0),(2,'wall',55,0),(2,'wall',56,0),
(2,'path',57,0),(2,'path',58,0),(2,'path',59,0),(2,'path',60,0),(2,'path',61,0),(2,'encounter',62,2),(2,'path',63,0),(2,'encounter',64,1);

INSERT INTO map_data (map, metadata, map_identifier, encounter_type) VALUES
(3,'encounter',1,1),(3,'path',2,0),(3,'path',3,0),(3,'path',4,0),(3,'wall',5,0),(3,'wall',6,0),(3,'wall',7,0),(3,'wall',8,0),
(3,'wall',9,0),(3,'wall',10,0),(3,'wall',11,0),(3,'path',12,0),(3,'wall',13,0),(3,'wall',14,0),(3,'wall',15,0),(3,'wall',16,0),
(3,'wall',17,0),(3,'wall',18,0),(3,'wall',19,0),(3,'path',20,0),(3,'encounter',21,3),(3,'encounter',22,4),(3,'wall',23,0),(3,'wall',24,0),
(3,'wall',25,0),(3,'encounter',26,4),(3,'encounter',27,3),(3,'path',28,0),(3,'wall',29,0),(3,'wall',30,0),(3,'wall',31,0),(3,'wall',32,0),
(3,'wall',33,0),(3,'wall',34,0),(3,'wall',35,0),(3,'path',36,0),(3,'encounter',37,3),(3,'encounter',38,4),(3,'wall',39,0),(3,'wall',40,0),
(3,'wall',41,0),(3,'wall',42,0),(3,'wall',43,0),(3,'path',44,0),(3,'wall',45,0),(3,'wall',46,0),(3,'wall',47,0),(3,'wall',48,0),
(3,'wall',49,0),(3,'encounter',50,4),(3,'encounter',51,3),(3,'path',52,0),(3,'wall',53,0),(3,'wall',54,0),(3,'wall',55,0),(3,'wall',56,0),
(3,'wall',57,0),(3,'wall',58,0),(3,'wall',59,0),(3,'path',60,0),(3,'path',61,0),(3,'encounter',62,3),(3,'encounter',63,5),(3,'encounter',64,1);

select count(*) from web2a.map_data;
create table logs (_id int(3) not null auto_increment primary key,
log varchar(255) not null,
action_type varchar(16) not null,
actor_id varchar(10) not null default '<System>'
);

create table combat_logs (_id int(3) not null auto_increment primary key,
log varchar(255) not null,
action_type varchar(16) not null,
actor_id varchar(10) not null default '<System>',
session_id int(3) not null
);