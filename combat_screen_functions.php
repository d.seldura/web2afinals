<?php
require_once("script_driver_DB.php");

function sprite($posX, $posY, $image, $name){
    $html='';
    $left = 63+($posX*80);
    $top = 66 + (80*7) - ($posY*80);
    $css = "<style> #".$name."{
        margin: 0;
        padding: 0;
        position: absolute;
        width: 80px;
        height: 80px;
        left:".$left."px;
        top: ".$top."px
    }</style>";
    $sprite = '<div id="'.$name.'"><img src="'.$image.'"></div>';
    echo $css.$sprite;
}

function getEnemiesFromDB(){
    $mobs ="SELECT * from mobs;";
    $DBL = new DB();
    $DBL->connect();
    $results = $DBL->getQuery($mobs);
    return $results;
}

function getAction(){
    if (isset($_GET['action']))
    return $_GET['action'];
}