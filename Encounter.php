<?php
if (!isset($_SESSION))
    session_start();
include_once('Combatant.php');
require_once("ItemHandler.php");
require_once("Logs.php");

class Encounter
{

    private $Mario;
    private $CurrentItems;
    private $Logs;
    function __construct(Combatant $protagonist, ItemHandler $items)
    {
        $this->Mario = $protagonist;
        $this->CurrentItems = $items;
        $this->Logs = new Logs();
    }

    //2 enemy,3 boss,4 treasure,5 key

    function evaulateEncounter($type)
    {
        switch ($type) {
            case 2:
                    $_SESSION['map_data'][$_SESSION['marioPosition'] - 1]['encounter_type'] = 0;
                    $_SESSION['map_data'][$_SESSION['marioPosition'] - 1]['metadata'] = 'path';
                    header('Location: combat_screen.php?isBoss=false');
                break;
            case 3:
                    $_SESSION['map_data'][$_SESSION['marioPosition'] - 1]['encounter_type'] = 0;
                    $_SESSION['map_data'][$_SESSION['marioPosition'] - 1]['metadata'] = 'path';
                    header('Location: combat_screen.php?isBoss=true');
                break;
            case 4:
                $_SESSION['map_data'][$_SESSION['marioPosition'] - 1]['encounter_type'] = 0;
                $_SESSION['map_data'][$_SESSION['marioPosition'] - 1]['metadata'] = 'path';
                $this->treasure();
                break;
            case 5:
                $this->Logs->send_log("Key obtained! The portal will now work.", "Treasure", "SYSTEM", 1);
                $_SESSION['map_data'][$_SESSION['marioPosition'] - 1]['encounter_type'] = 0;
                $_SESSION['map_data'][$_SESSION['marioPosition'] - 1]['metadata'] = 'path';
            default:
                # code...
                break;
        }
    }

    function treasure()
    {
        //   1 +1 Bomb
        //   2 +1 Potion
        //   3 +10 Max HP
        //   4 +5 Damage
        //   5 +25 Max HP
        //   6 +5 Max HP

        switch (rand(1, 6)) {
            case 1:
                $this->CurrentItems->addBomb();
                $this->Logs->send_log("Treasure Obtained! +1 Bomb", "Treasure", "SYSTEM", 1);
                break;
            case 2:
                $this->CurrentItems->addPotion();
                $this->Logs->send_log("Treasure Obtained! +1 Potion", "Treasure", "SYSTEM", 1);
                break;
            case 3:
                $this->Mario->increaseMaxHP(10);
                $this->Logs->send_log("Buff Obtained! +10 Max HP", "Treasure", "SYSTEM", 1);
                break;
            case 4:
                $this->Mario->increaseAttack(5);
                $this->Logs->send_log("Buff Obtained! +5 Damage", "Treasure", "SYSTEM", 1);
                break;
            case 5:
                $this->Mario->increaseMaxHP(25);
                $this->Logs->send_log("Buff Obtained! +25 Max HP", "Treasure", "SYSTEM", 1);
                break;
            case 6:
                $this->Mario->increaseMaxHP(5);
                $this->Logs->send_log("Buff Obtained! +5 Max HP", "Treasure", "SYSTEM", 1);
                break;
        }
    }

    function getObjects()
    {
        return ['Mario' => $this->Mario, 'CurrentItems' => $this->CurrentItems];
    }
}
