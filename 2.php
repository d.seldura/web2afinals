<?php
require_once("SessionHandler.php");
require_once("Cart.php");
require_once("Item.php");
use ShoppingCart\SessionHandler\SessionHandler;
use ShoppingCart\Cart\Cart;
use ShoppingCart\Item\Item;
$SH = new SessionHandler();
$cartData = array();
array_push($cartData, new Item(1,"Assassin's Creed: Orgins",59.99), new Item(2,"Control",23.99), new Item(3,"Borderlands 3",44.99));
$Cart = new Cart($cartData);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Games R Us</title>
</head>
<body>
    <h1>Games-R-US</h1>
    <hr>
    <h3>Select a game to purchase</h3>
    <select name = "games">
            <option value = "Assassin's Creed: Orgins" selected>Assassin's Creed: Orgins</option>
            <option value = "Control">Control</option>
            <option value = "Borderlands 3">Borderlands 3</option>
    </select>
    <label for="amount">Quantity</label>
    <input type="number" name="quantity" >
    <hr>
    <br>
    <table>
    <tr>
    <th>Item #</th>
    <th>Item Name</th>
    <th>Price</th>
    <th>Quantity</th>
    <th>Amount</th>
    </tr>
    <tr><?php
    //insert cart item
    ?>
    
    </tr>
    </table>
    <h3>Total Amount:
    <?php
    //insert total script here
    ?>
    </h3>
    <button>Add to Cart</button><button>Check Out</button>
</body>
</html>