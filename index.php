<?php
session_start();

require_once("Combatant.php");
require_once("ItemHandler.php");
require_once("script_driver_DB.php");
require_once("main_functions.php");



if (isset($_GET['action'])) {
    $action = $_GET['action'];
    if ($action == 'start') {
        session_destroy();
        $DB = new DB();
        $DB->connect();
        $DB->setQuery('TRUNCATE TABLE logs');
        $DB->setQuery('TRUNCATE TABLE combat_logs');
        header('Location: main.php');
    }
    if ($action == 'continue') {
        header('Location: main.php');
    }
}

$Start = new Image('button_start.png', 'start_button', 90, 450);
$Continue = new Image('button_continue.png', 'continue_button', 405, 450);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Super Mario and The Sand That Got Away</title>
    <link rel="stylesheet" type="text/css" href="index_css.css"> 
</head>

<body>
    <a href='index.php?action=start'>
        <?php
        $Start->render();
        ?></a>
    <a href='index.php?action=continue'>
        <?php
        $Continue->render();
        ?></a>
    <div id="info">
        <p>Mama Mia! Mario decided to take a vacation from stomping Goombas and booked a flight to the Shifting Sands Resort. </p>
        <p>It’s hard being a hero.</p> 
        <p>So there he was, sipping on a morning frozen margarita at Oasis Resort, when suddenly a weary traveler approaches after doing a wallrun to get to his balcony.<p>
        <p> “Help, Mario! Bowser is trying to steal the Sands of Time from the pyramid yonder. Can you help?”<p>
        <p> Naturally Mario sprang into action with a “Yahaa!”<p>
        <p>Mario:  "I wonder where that dude came from… he had two scimitars, a handsome face and one fine scarf."</p>

        <p> A fan game by Zenith Development Group</p>

    </div>
</body>

</html>