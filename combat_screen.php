<?php
session_start();
require_once("ItemHandler.php");
require_once("combat_screen_functions.php");
require_once("Logs.php");
require_once("Encounter.php");
require_once("Combatant.php");

if (isset($_GET['isBoss'])) {
    $isBoss = $_GET['isBoss'];
}

$log_handler = new LogsCombat();
$game_logs = new Logs();


if (!isset($_SESSION['enemies'])) {
    $_SESSION['enemies'] = getEnemiesFromDB();
}
if (!isset($_SESSION['combatStart']))
    $_SESSION['combatStart'] = false;

if (!$_SESSION['combatStart']) {
    if ($isBoss == "true") {
        $roll_mob = rand(3, 5);
    } else
        $roll_mob = rand(0, 2);
    $_SESSION['antagonist'] = serialize(new Combatant(
        $_SESSION['enemies'][$roll_mob]['hp'],
        $_SESSION['enemies'][$roll_mob]['hp'],
        $_SESSION['enemies'][$roll_mob]['attack'],
        $_SESSION['enemies'][$roll_mob]['defense'],
        $_SESSION['enemies'][$roll_mob]['mobName'],
        $_SESSION['enemies'][$roll_mob]['image']
    ));
    $_SESSION['combatStart'] = true;
    $_SESSION['combatEnd'] = false;
    if (!isset($_SESSION['combat_session_id']))
        $_SESSION['combat_session_id'] = 1;
    else
        $_SESSION['combat_session_id'] += 1;

    $CombatLog = new LogsCombat;
    $CombatLog->send_log("COMBAT STARTS! (A)ttack, (D)efend, (H)eal, (B)omb", "Combat", "SYSTEM", $_SESSION["combat_session_id"]);
}

$Mario = unserialize($_SESSION['protagonist']);
$Mario->setImage("combat_sprite_ready.png");
$Enemy = unserialize($_SESSION['antagonist']);
$Items = unserialize($_SESSION['items']);
$Encounter = new Encounter($Mario, $Items);

?>
<script>
    function gameEndLoss() {
        alert('Mario has been defeated! GAME OVER');
        window.location.replace("index.php");
    }

    function combatWin() {

        alert('Enemy has been defeated! Mario is fully healed!');
        window.location.replace("main.php");
    }
</script>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Combat</title>
    <link rel="stylesheet" href="combat_screen_css.css">
</head>

<body>
    <?php
    //evaluate the combat functions here
    if ($_SESSION['combatStart']) {
        $action = getAction();
        if ($action == "attack") {
            $Mario->setImage("combat_sprite_attack.png");
            $Enemy->take_damage($Mario->attack());
            if (!$Enemy->deathStatus()) {
                $Mario->take_damage($Enemy->attack());
                if ($Mario->deathStatus()) {
                    $_SESSION['combatStart'] = false;
                    $_SESSION['combatEnd'] = true;
                    echo "<script>gameEndLoss();</script>";
                } //game ends, mario dies
            } else {
                $_SESSION['combatStart'] = false;
                $_SESSION['combatEnd'] = true; //combat ends, mario wins
                $game_logs->send_log("Mario has defeated " . $Enemy->getStats()['name'], "WIN", "COMBAT");
                $Mario->combatEndHeal();
                $Encounter->treasure();
                $_SESSION['protagonist'] = serialize($Encounter->getObjects()['Mario']);
                $_SESSION['items'] = serialize($Encounter->getObjects()['CurrentItems']);
                echo "<script>combatWin()</script>";
            }
        }
        if ($action == "defend") {
            $Mario->setImage("combat_sprite_defend.png");
            $Mario->defend($Enemy->attack());
            if ($Mario->deathStatus()) {
                $_SESSION['combatStart'] = false;
                $_SESSION['combatEnd'] = true;
                echo "<script>gameEndLoss()</script>";
            } //game ends, mario dies
        }
        if ($action == "heal") {
            $Mario->setImage("combat_sprite_heal.png");
            $Mario->heal($Enemy->attack());
            $Items->usePotion();
            if ($Mario->deathStatus()) {
                $_SESSION['combatStart'] = false;
                $_SESSION['combatEnd'] = true;
                echo "<script>gameEndLoss()</script>";
            } //game ends, mario dies
        }
        if ($action == "bomb") {
            $Mario->setImage("combat_sprite_bomb.png");
            $Mario->bomb();
            $Items->useBomb();
            $Enemy->bombed();
            if (!$Enemy->deathStatus()) {
                $Mario->take_damage($Enemy->attack());
                if ($Mario->deathStatus()) {
                    $_SESSION['combatStart'] = false;
                    $_SESSION['combatEnd'] = true;
                    echo "<script>gameEndLoss();</script>";
                } //game ends, mario dies
            } else {
                $_SESSION['combatStart'] = false;
                $_SESSION['combatEnd'] = true; //combat ends, mario wins
                $game_logs->send_log("Mario has defeated " . $Enemy->getStats()['name'], "WIN", "COMBAT");
                $Mario->combatEndHeal();
                $Encounter->treasure();
                $_SESSION['protagonist'] = serialize($Encounter->getObjects()['Mario']);
                $_SESSION['items'] = serialize($Encounter->getObjects()['CurrentItems']);
                echo "<script>combatWin()</script>";
            }
        }
        $_SESSION['antagonist'] = serialize($Enemy);
        $_SESSION['protagonist'] = serialize($Mario);
        $_SESSION['items'] = serialize($Items);
    }
    ?>
    <div id="map">
        <div id="combat-announcer">
            <div>MARIO</div>
            <div>VS</div>
            <div>
                <?php
                echo $Enemy->getStats()['name'];
                ?></div>
        </div>
        <img src="bg_combat.png">
    </div>
    <div id="status-hp"><?php echo $Mario->getstats()["hp"] ?></div>
    <div id="status-potion"><?php echo $Items->getItems()['potions'] ?>/3</div>
    <div id="status-bomb"><?php echo $Items->getItems()['bombs'] ?>/3</div>
    <div id="controls">
        <a href="http://localhost/web2afinals/combat_screen.php?action=attack<?php echo "&isBoss=" . $isBoss ?>"><img src="combat_attack.png"></a>
        <a href="http://localhost/web2afinals/combat_screen.php?action=defend<?php echo "&isBoss=" . $isBoss ?>"><img src="combat_defend.png"></a>
        <?php

        if ($Items->getItems()['potions'])
            echo '<a href="http://localhost/web2afinals/combat_screen.php?action=heal&isBoss=' . $isBoss . '"><img src="combat_heal.png"></a>';
        if ($Items->getItems()['bombs'])
            echo '<a href="http://localhost/web2afinals/combat_screen.php?action=bomb&isBoss=' . $isBoss . '"><img src="combat_bomb.png"></a>';
        ?></div>
    <div id="logs">
        <?php
        $log_handler->get_logs($_SESSION["combat_session_id"]);
        ?>
    </div>
    </div>
    <div class="combatant protagonist">
        <?php
        $Mario->render();
        ?>
    </div>
    <div class="combatant antagonist">
        <?php
        $Enemy->render();
        ?>
    </div>
    <script>
        window.onkeydown = function(event) {
            switch (event.keyCode) {
                case 65:
                    window.location.href = "combat_screen.php?action=attack&<?php echo "&isBoss=" . $isBoss ?>";
                    break;
                case 68:
                    window.location.href = "combat_screen.php?action=defend&<?php echo "&isBoss=" . $isBoss ?>";
                    break;
                    <?php if ($Items->getItems()['potions']) echo
                        'case 72:
                    window.location.href = "combat_screen.php?action=heal&isBoss=' . $isBoss . '";
                    break;'; ?>
                    <?php if ($Items->getItems()['bombs']) echo
                        'case 66:
                    window.location.href = "combat_screen.php?action=bomb&isBoss=' . $isBoss . '";
                    break;'; ?>

                default:
                    break;
            }
        }
    </script>
</body>

</html>