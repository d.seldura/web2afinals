<?php
require_once("Logs.php");

class Combatant
{
    private $hp;
    private $max_hp;
    private $name;
    private $attack;
    private $defense;
    private $image;
    private $isDead;

    public function __construct($hp, $max_hp, $attack, $defense, $name, $image)
    {
        $this->hp = $hp;
        $this->max_hp = $max_hp;
        $this->attack = $attack;
        $this->defense = $defense;
        $this->name = $name;
        $this->image = $image;
        $this->isDead = false;
    }

    public function setImage($newImage)
    {
        $this->image = $newImage;
    }

    public function attack()
    {
        $logger = new LogsCombat();
        $damageVal = rand(intval($this->attack * 0.7), $this->attack);
        $logger->send_log($this->name . " attacks for " . $damageVal, "COMBAT", "<System>", $_SESSION["combat_session_id"]);
        //echo "<script>alert(" . $damageVal . ");</script>";
        return $damageVal;
        //randomly attack based on a floor of 70% to 100% damage 
    }

    public function take_damage($incoming_damage)
    {
        $logger = new LogsCombat();
        $defMultiplier = 1 - ($this->defense / 100);
        $actual_damage = intval($incoming_damage * $defMultiplier);
        $this->hp -= $actual_damage;
        $this->deathEval($this->hp);
        if ($this->isDead) {
            $logger->send_log($this->name . " takes " . $actual_damage . " lethal damage. HP: " . $this->hp, "COMBAT", "<System>", $_SESSION["combat_session_id"]);
        } else $logger->send_log($this->name . " takes " . $actual_damage . " damage. HP: " . $this->hp, "COMBAT", "<System>", $_SESSION["combat_session_id"]);
        //echo "<script>alert(" . $incoming_damage . $actual_damage . $this->hp . $defMultiplier . ");</script>";
        //defense means % damage reduction using defense value/200.
        //eg a defense value of 25 means 25% reduction of damage taken rounded to int
    }

    public function defend($incoming_damage)
    {
        $logger = new LogsCombat();
        $defMultiplier = 1 - ((1.5 * $this->defense) / 100);
        $actual_damage = intval($incoming_damage * $defMultiplier);
        $this->hp -= $actual_damage;
        $this->deathEval($this->hp);
        if ($this->isDead) {
            $logger->send_log($this->name . " defends." . " Takes " . $actual_damage . " lethal damage from " . $incoming_damage . " attack" . " HP: " . $this->hp, "COMBAT", "<System>", $_SESSION["combat_session_id"]);
        } else $logger->send_log($this->name . " defends." . " Takes " . $actual_damage . " damage from " . $incoming_damage . " attack" . " HP: " . $this->hp, "COMBAT", "<System>", $_SESSION["combat_session_id"]);
        //defending increases your defense by 50%
        //which means a def value of 20 becomes 30 and incoming damage is reduced by 30%
    }

    public function heal($incoming_damage)
    {
        $logger = new LogsCombat();
        $defMultiplier = 1 - ($this->defense / 100);
        $actual_damage = intval($incoming_damage * $defMultiplier);
        $this->hp += intval($this->hp * 0.5);
        if($this->hp>$this->max_hp)
            $this->hp=$this->max_hp;
        $logger->send_log($this->name . " heals for +" . intVal($this->hp * 0.5) . " HP. Current HP: " . $this->hp, "COMBAT", "<System>", $_SESSION["combat_session_id"]);
        $this->hp -= $actual_damage;
        $this->deathEval($this->hp);
        if ($this->isDead) {
            $logger->send_log($this->name . " takes " . $actual_damage . " lethal damage. HP: " . $this->hp, "COMBAT", "<System>", $_SESSION["combat_session_id"]);
        } else $logger->send_log($this->name . " takes " . $actual_damage . " damage. HP: " . $this->hp, "COMBAT", "<System>", $_SESSION["combat_session_id"]);
    }

    public function bomb()
    {
        $logger = new LogsCombat();
        $logger->send_log($this->name . " throws a bomb.", "COMBAT", "<System>", $_SESSION["combat_session_id"]);
    }

    public function bombed()
    {
        $logger = new LogsCombat();
        $actual_damage = 100;
        $this->hp -= $actual_damage;
        $this->deathEval($this->hp);
        if ($this->isDead) {
            $logger->send_log($this->name . " is hit by a bomb. " . $actual_damage . " lethal damage. HP: " . $this->hp, "COMBAT", "<System>", $_SESSION["combat_session_id"]);
        } else $logger->send_log($this->name . " is hit by a bomb. " . $actual_damage . " pure damage. HP: " . $this->hp, "COMBAT", "<System>", $_SESSION["combat_session_id"]);
    }

    public function render()
    {
        $html = '
    <div class="combat-stats">
        <img src="icon_armor.png">
        <span>' . $this->defense . '</span>
        <img src="icon_attack.png">
        <span>' . $this->attack . '</span>
    </div>
    <div class="combat-sprite">
        <img src="' . $this->image . '">
    </div>
    <div class="combat-stats" id="hp">
        <span>' . $this->hp . '</span>
        </div>';
        echo $html;
    }
    public function getStats()
    {
        return [
            "hp" => $this->hp,
            "max_hp" => $this->max_hp,
            "attack" => $this->attack,
            "defense" => $this->defense,
            "name" => $this->name
        ];
    }

    public function increaseMaxHP($amount){
        $this->max_hp+=$amount;
    }

    public function increaseAttack($amount){
        $this->attack+=$amount;
    }

    public function combatEndHeal(){
        $this->hp=$this->max_hp;
    }

    public function deathStatus()
    {
        return $this->isDead;
    }

    public function deathEval($hp)
    {
        if ($hp <= 0) {
            $this->hp = 0;
            $this->isDead = true;
        } else $this->isDead = false;
    }
}
